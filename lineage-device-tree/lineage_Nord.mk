#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from Nord device
$(call inherit-product, device/oneplus/Nord/device.mk)

PRODUCT_DEVICE := Nord
PRODUCT_NAME := lineage_Nord
PRODUCT_BRAND := OnePlus
PRODUCT_MODEL := Nord_IND
PRODUCT_MANUFACTURER := oneplus

PRODUCT_GMS_CLIENTID_BASE := android-oneplus

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="lito-user 12 RKQ1.211119.001 1694620742520 release-keys"

BUILD_FINGERPRINT := qti/lito/lito:12/RKQ1.211119.001/1694620742520:user/release-keys
