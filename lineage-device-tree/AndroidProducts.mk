#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_Nord.mk

COMMON_LUNCH_CHOICES := \
    lineage_Nord-user \
    lineage_Nord-userdebug \
    lineage_Nord-eng
